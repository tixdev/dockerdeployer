﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Renci.SshNet;

namespace SSH
{
    class Program
    {
        private static IConfiguration _configuration;
        
        private static string DockerCommand = "/share/CACHEDEV2_DATA/.qpkg/container-station/bin/docker";
        //private const string DockerCommand = "sudo docker";

        static void Main(string[] args)
        {
            BuildConfiguration();

            var connection = new
                {Host = _configuration["Host"], Username = _configuration["User"], Password = _configuration["Password"]};
            var appName = _configuration["AppName"];
            var imageName = $"{appName}_image";
            var dockerfile = "Dockerfile";

            var scpFileUpload = new ScpFileUpload(connection.Host, connection.Username, connection.Password);

            var destinationFilePath = "/root/";
            scpFileUpload.UploadFile(dockerfile, destinationFilePath);

            var ssh = new SshClient(connection.Host, connection.Username, connection.Password);
            ssh.Connect();
            Console.WriteLine("SSH connected...");

            CreateDataDirectory(ssh, appName);

            var containers = GetContainers(ssh);

            var runningContainer = containers.FirstOrDefault(c => c.Names == appName);

            if (runningContainer != null)
                StopContainer(ssh, runningContainer);

            var images = GetImages(ssh);
            foreach (var imageModel in images)
            {
                if(imageModel.Repository == "<none>" || imageModel.Repository == imageName)
                    RemoveImage(ssh, imageModel);
            }
            
            BuildImage(ssh, imageName);

            RunContainer(ssh, appName, imageName);

            SSHDisconnect(ssh);

            Console.WriteLine("Completed!");
        }

        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            _configuration = builder.Build();
        }

        private static void CreateDataDirectory(SshClient ssh, string appName)
        {
            Console.WriteLine($"Create volume");

            var response = ssh.RunCommand($"sudo mkdir /data");
            response = ssh.RunCommand($"sudo mkdir /data/{appName}");

            Console.WriteLine(response.Result);
            
            if (!string.IsNullOrWhiteSpace(response.Error) && !response.Error.Contains("File exists"))
                throw new Exception(response.Error);
        }

        private static void SSHDisconnect(SshClient ssh)
        {
            Console.WriteLine("SSH disconnecting...");
            Task.Run(ssh.Disconnect);
            Console.WriteLine("SSH disconnected...");
        }

        private static void BuildImage(SshClient ssh, string imageName)
        {
            Console.WriteLine($"Build docker image {imageName}...this operation can take a while");

            var response = ssh.RunCommand($"cd /tmp\n");

            if (!string.IsNullOrWhiteSpace(response.Error))
            {
                Console.WriteLine(response.Error);
                return;
            }

            response = ssh.RunCommand($"{DockerCommand} build . -t {imageName}");

            Console.WriteLine(response.Result);
            
            if (!string.IsNullOrWhiteSpace(response.Error))
                throw new Exception(response.Error);
        }

        private static void RunContainer(SshClient ssh, string appName, string imageName)
        {
            Console.WriteLine($"Run container {appName}");

            var response = ssh.RunCommand(
                $"{DockerCommand} run --rm --name {appName} -d -p 30101:80 -v /data/{appName}:/data {imageName}");

            if (!string.IsNullOrWhiteSpace(response.Error)) Console.WriteLine(response.Error);

            Console.WriteLine($"{appName} started...");
        }

        private static void StopContainer(SshClient ssh, ContainerModel c)
        {
            Console.WriteLine($"Stopping container {c.Id} - {c.Names}");

            var response = ssh.RunCommand($"{DockerCommand} stop {c.Id}");

            Console.WriteLine(response.Result);
            
            if (!string.IsNullOrWhiteSpace(response.Error)) Console.WriteLine(response.Error);
        }

        private static List<ContainerModel> GetContainers(SshClient ssh)
        {
            var runningContainers = ssh.RunCommand($@"{DockerCommand} container ls --format='{{{{json .}}}}'");

            Console.WriteLine(runningContainers.Result);
            
            var containers = new List<ContainerModel>();

            runningContainers.Result.Split('\n', StringSplitOptions.RemoveEmptyEntries).ToList()
                .ForEach(c => containers.Add(JsonConvert.DeserializeObject<ContainerModel>(c)));

            return containers;
        }
        
        private static List<ImageModel> GetImages(SshClient ssh)
        {
            var images = ssh.RunCommand($@"{DockerCommand} images --format='{{{{json .}}}}'");

            Console.WriteLine(images.Result);
            
            var imageModels = new List<ImageModel>();

            images.Result.Split('\n', StringSplitOptions.RemoveEmptyEntries).ToList()
                .ForEach(c => imageModels.Add(JsonConvert.DeserializeObject<ImageModel>(c)));

            return imageModels;
        }
        
        private static void RemoveImage(SshClient ssh, ImageModel model)
        {
            Console.WriteLine($"Removing image {model.Id} - {model.Repository} - {model.Tag}");
            
            var response = ssh.RunCommand($@"{DockerCommand} rmi {model.Id} --force");

            Console.WriteLine(response.Result);
        }
    }
}