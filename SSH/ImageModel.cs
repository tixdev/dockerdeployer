namespace SSH
{
    public class ImageModel
    {
        public string Id { get; set; }
        public string Repository { get; set; }
        public string Tag { get; set; }
    }
}