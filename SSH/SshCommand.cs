using System;
using System.Collections.Generic;
using System.Threading;
using Renci.SshNet;

namespace SSH
{
    public static class SshCommand
    {
        public static List<string> ExecuteCommand(this ShellStream shellStream, string command)
        {
            var buffer = new List<string>();
            
            string ReadLine()
            {
                var line = shellStream.Read();
                buffer.Add(line);
                Console.Write(line);

                return line;
            }

            while (!string.IsNullOrWhiteSpace(ReadLine())) Thread.Sleep(1000);
            
            shellStream.WriteLine(command);
            
            while (!string.IsNullOrWhiteSpace(ReadLine())) Thread.Sleep(1000);

            return buffer;
        }
    }
}