using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Renci.SshNet;

namespace SSH
{
    public class ScpFileUpload
    {
        private readonly ScpClient _scpClient;

        public ScpFileUpload(string host, string username, string password)
        {
            _scpClient = new ScpClient(host, username, password);
        }

        public void UploadFile(string sourceFilePath, string destinationFilePath)
        {
            Connect();

            UploadFileImpl(sourceFilePath, destinationFilePath);
        }

        private void Connect()
        {
            _scpClient.Connect();
            Console.WriteLine("SCP Connected");
        }

        private void UploadFileImpl(string sourceFilePath, string destinationFilePath)
        {
            var uploaded = false;

            Task.Run(() =>
            {
                _scpClient.Uploading += (sender, eventArgs) =>
                {
                    if (eventArgs.Size != eventArgs.Uploaded) return;

                    Console.WriteLine("SCP Disconnecting...");
                    uploaded = true;
                    _scpClient.Disconnect();
                };

                BeginUpload(sourceFilePath, destinationFilePath);
            });

            while (!uploaded) Thread.Sleep(500);
        }

        private void BeginUpload(string sourceFilePath, string destinationFilePath)
        {
            var di = new FileInfo(sourceFilePath);
            _scpClient.Upload(di, destinationFilePath);
        }
    }
}